﻿using mishar.Lesson4.Acme.UI.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mishar.Lesson4.Acme.UI
{
    public static class AcmePages
    {
        public static CreateAccountePage CreateAccountePage => new CreateAccountePage();
        public static AuthenticationPage AuthenticationPage => new AuthenticationPage();
        public static HomePage HomePage => new HomePage();
    }
}
