﻿using mishar.Lesson4.Acme.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace mishar.Lesson4.Acme.UI.Pages
{
    public class CreateAccountePage
    {
        #region PageMapping
        private IWebElement Email => DriverManager.Current.FindElement(By.Id("email"));
        private IWebElement Gender1 => DriverManager.Current.FindElement(By.Id("id_gender1"));
        //private IWebElement Gender2 => DriverManager.Current.FindElement(By.Id("id_gender2"));
        private IWebElement CustomerFirstName => DriverManager.Current.FindElement(By.Id("customer_firstname"));
        private IWebElement CustomerLastName => DriverManager.Current.FindElement(By.Id("customer_lastname"));
        private IWebElement Password => DriverManager.Current.FindElement(By.Id("passwd"));
        private IWebElement DayOfBirth => DriverManager.Current.FindElement(By.Id("days"));
        private IWebElement MonthOfBirth => DriverManager.Current.FindElement(By.Id("months"));
        private IWebElement YearOfBirth => DriverManager.Current.FindElement(By.Id("years"));
        private IWebElement NewsLetterClick => DriverManager.Current.FindElement(By.Id("newsletter"));
        private IWebElement OffersClick => DriverManager.Current.FindElement(By.Id("optin"));
        private IWebElement FirstName => DriverManager.Current.FindElement(By.Id("firstname"));
        private IWebElement LastName => DriverManager.Current.FindElement(By.Id("lastname"));
        private IWebElement Company => DriverManager.Current.FindElement(By.Id("company"));
        private IWebElement Address1 => DriverManager.Current.FindElement(By.Id("address1"));
        private IWebElement City => DriverManager.Current.FindElement(By.Id("city"));
        private IWebElement State => DriverManager.Current.FindElement(By.Id("id_state"));
        private IWebElement PostCode => DriverManager.Current.FindElement(By.Id("postcode"));
        private IWebElement Country => DriverManager.Current.FindElement(By.Id("id_country"));
        private IWebElement OtherInfo => DriverManager.Current.FindElement(By.Id("other"));
        private IWebElement HomePhone => DriverManager.Current.FindElement(By.Id("phone"));
        private IWebElement MobilePhone => DriverManager.Current.FindElement(By.Id("phone_mobile"));
        private IWebElement AddressAlias => DriverManager.Current.FindElement(By.Id("alias"));
        private IWebElement SubmitAccount => DriverManager.Current.FindElement(By.Id("submitAccount"));
        #endregion

        #region PageObjects
        public string GetEmail()
        {
            var email = Email.GetAttribute("value");
            return email;
        }
        public void Gender1Click()
        {
            Gender1.Click();
        }
        public void SetCustomerFirstName(string value)
        {
            CustomerFirstName.SendKeys(value);
        }
        public void SetCustomerLastName(string value)
        {
            CustomerLastName.SendKeys(value);
        }
        public void SetPassword(string value)
        {
            Password.SendKeys(value);
        }
        public void SetDayOfBirth(int value)
        {
            SelectElement birthDay = new SelectElement(DayOfBirth);
            birthDay.SelectByIndex(value);
        }
        public void SetMonthOfBirth(int value)
        {
            SelectElement birthMonth = new SelectElement(MonthOfBirth);
            birthMonth.SelectByIndex(value);
        }
        public void SetYearOfBirth(int value)
        {
            SelectElement birthYear = new SelectElement(YearOfBirth);
            birthYear.SelectByIndex(value);
        }
        public void SetNewsLetterClick()
        {
            NewsLetterClick.Click();
        }
        public void SetOffersClick()
        {
            OffersClick.Click();
        }
        public void SetFirstName(string value)
        {
            FirstName.SendKeys(value);
        }
        public void SetLastName(string value)
        {
            LastName.SendKeys(value);
        }
        public void SetCompany(string value)
        {
            Company.SendKeys(value);
        }
        public void SetAddress1(string value)
        {
            Address1.SendKeys(value);
        }
        public void SetCity(string value)
        {
            City.SendKeys(value);
        }
        public void SetState(string value)
        {
            State.SendKeys(value);
        }
        public void SetPostCode(string value)
        {
            PostCode.SendKeys(value);
        }
        public void SetCountry(string value)
        {
            Country.SendKeys(value);
        }
        public void SetOtherInfo(string value)
        {
            OtherInfo.SendKeys(value);
        }
        public void SetHomePhone(string value)
        {
            HomePhone.SendKeys(value);
        }
        public void SetMobilePhone(string value)
        {
            MobilePhone.SendKeys(value);
        }
        public void SetAddressAlias(string value)
        {
            AddressAlias.Clear();
            AddressAlias.SendKeys(value);
        }
        public void ClickSubmitAccount()
        {
            SubmitAccount.Click();
            Thread.Sleep(2000);
        }
        #endregion
    }
}
