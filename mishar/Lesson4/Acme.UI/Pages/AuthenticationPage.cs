﻿using mishar.Lesson4.Acme.Framework;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace mishar.Lesson4.Acme.UI.Pages
{
    public class AuthenticationPage
    {
        #region PageMapping
        private IWebElement Email => DriverManager.Current.FindElement(By.Id("email_create"));
        private IWebElement CreateAccount => DriverManager.Current.FindElement(By.Id("SubmitCreate"));
        private IWebElement CreateAccountError => DriverManager.Current.FindElement(By.Id("create_account_error"));
        #endregion

        #region PageObjects
        public void ClickCreateAccount()
        {
            CreateAccount.Click();
            Thread.Sleep(2000);
        }
        public string GetError()
        {
            var error = CreateAccountError.Text;
            return error;
        }
        public void SetEmail(string email)
        {
            Email.SendKeys(email);
        }
        #endregion
    }
}
