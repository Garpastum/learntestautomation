﻿using mishar.Lesson4.Acme.Framework;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mishar.Lesson4.Acme.UI.Pages
{
    public class HomePage
    {
        #region PageMapping
        private IWebElement SignInLink => DriverManager.Current.FindElement(By.ClassName("login"));
        #endregion

        #region PageObjects
        public void ClickSignIn()
        {
            SignInLink.Click();
        }
        #endregion
    }
}
