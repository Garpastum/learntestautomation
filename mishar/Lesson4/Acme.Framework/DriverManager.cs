﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mishar.Lesson4.Acme.Framework
{
    class DriverManager
    {
        private static IWebDriver _driver;
        public static IWebDriver Current
        {
            get
            {
                if (_driver==null)
                {
                    _driver = new ChromeDriver();
                }
                return _driver;
            }
        }
    }
}
