﻿using mishar.Lesson4.Acme.Framework;
using mishar.Lesson4.Acme.UI;
using mishar.Lesson4.Acme.UI.Common;
using NUnit.Framework;
using NUnit.Framework.Interfaces;
using OpenQA.Selenium;
using System;
using System.IO;
using System.Reflection;

namespace mishar.Lesson4.Acme.Tests
{
    public class RegisterAccountFixture
    {
        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            DriverManager.Current.Manage().Window.Maximize();
        }
        [SetUp]
        public void SetUp()
        {
            DriverManager.Current.Navigate().GoToUrl("http://automationpractice.com/index.php");
        }
        [TearDown]
        public void TearDown()
        {
            if (TestContext.CurrentContext.Result.Outcome.Equals(ResultState.Failure))
            {
                string filename = $"{TestContext.CurrentContext.Test.Name} - {DateTime.Now:yyyy.MM.dd-h.m.ss}";
                FileInfo domPath = new FileInfo($"{Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)}\\dom\\{filename}.txt");
                Directory.CreateDirectory(domPath.DirectoryName);
                File.WriteAllText(domPath.FullName, DriverManager.Current.PageSource);

                FileInfo screenshotsPath = new FileInfo($"{Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)}\\screenshots\\{filename}.png");
                Directory.CreateDirectory(screenshotsPath.DirectoryName);
                ITakesScreenshot screenshotTaker = (ITakesScreenshot)DriverManager.Current;
                Screenshot screenshot = screenshotTaker.GetScreenshot();
                screenshot.SaveAsFile(screenshotsPath.FullName, ScreenshotImageFormat.Png);
            }
        }
        [OneTimeTearDown]
        public void OneTimeTearDown()
        {
            DriverManager.Current.Quit();
        }

        [Test]
        public void VerifyCreateAccountWithEmptyEmail()
        {
            AcmePages.HomePage.ClickSignIn();
            AcmePages.AuthenticationPage.ClickCreateAccount();
            var actualError = AcmePages.AuthenticationPage.GetError();
            Assert.AreEqual(AcmeStringResources.InvalidEmailAddressError, actualError);

        }
        [Test]
        public void VerifyCreateAccountWithInvalidEmail()
        {
            AcmePages.HomePage.ClickSignIn();
            AcmePages.AuthenticationPage.SetEmail("blablabla");
            AcmePages.AuthenticationPage.ClickCreateAccount();
            var actualError = AcmePages.AuthenticationPage.GetError();
            Assert.AreEqual(AcmeStringResources.InvalidEmailAddressError, actualError);
        }
        [Test]
        public void VerifyCreateAccountWithValidEmail()
        {
            var validEmail = "mailname@gmail.com";
            AcmePages.HomePage.ClickSignIn();
            AcmePages.AuthenticationPage.SetEmail(validEmail);
            AcmePages.AuthenticationPage.ClickCreateAccount();

            var actualEmail = AcmePages.CreateAccountePage.GetEmail();

            Assert.AreEqual(validEmail, actualEmail);
            //throw new NotImplementedException("Test should be implemented.");
        }
        [Test]
        public void SuccessRegistration()
        {
            var expectedUrl = "http://automationpractice.com/index.php?controller=my-account";
            AcmePages.HomePage.ClickSignIn();
            AcmePages.AuthenticationPage.SetEmail("blablabla1@gmail.com");
            AcmePages.AuthenticationPage.ClickCreateAccount();


            var crAccP = AcmePages.CreateAccountePage;
            crAccP.Gender1Click();
            crAccP.SetCustomerFirstName("John");
            crAccP.SetCustomerLastName("Smith");
            crAccP.SetPassword("asdfg");
            crAccP.SetDayOfBirth(5);
            crAccP.SetMonthOfBirth(12);
            crAccP.SetYearOfBirth(5);
            crAccP.SetNewsLetterClick();
            crAccP.SetOffersClick();
            crAccP.SetFirstName("Suzen");
            crAccP.SetLastName("Delgado");
            crAccP.SetCompany("Microsoft");
            crAccP.SetAddress1("Central street, 1");
            crAccP.SetCity("Rivne");
            crAccP.SetState("New York");
            crAccP.SetPostCode("33024");
            crAccP.SetCountry("Ukraine");
            crAccP.SetOtherInfo("Other info blablabla");
            crAccP.SetHomePhone("0671234567");
            crAccP.SetMobilePhone("0981234567");
            crAccP.SetAddressAlias("Soborna 1");
            crAccP.ClickSubmitAccount();

            Assert.AreEqual(expectedUrl, DriverManager.Current.Url);
        }
    }
}
