﻿using mishar.Lesson4.Acme.Framework;
using mishar.Lesson4.Acme.UI;
using mishar.Lesson4.Acme.UI.Common;
using NUnit.Framework;
using NUnit.Framework.Interfaces;
using OpenQA.Selenium;
using System;
using System.IO;
using System.Reflection;

namespace mishar.Lesson4.Acme.Tests
{
    public class CreateAccountFixture
    {
        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            DriverManager.Current.Manage().Window.Maximize();
        }
        [SetUp]
        public void SetUp()
        {
            DriverManager.Current.Navigate().GoToUrl("http://automationpractice.com/index.php");
        }
        [TearDown]
        public void TearDown()
        {
            if (TestContext.CurrentContext.Result.Outcome.Equals(ResultState.Failure))
            {
                string filename = $"{TestContext.CurrentContext.Test.Name} - {DateTime.Now:yyyy.MM.dd-h.m.ss}";
                FileInfo domPath = new FileInfo($"{Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)}\\dom\\{filename}.txt");
                Directory.CreateDirectory(domPath.DirectoryName);
                File.WriteAllText(domPath.FullName, DriverManager.Current.PageSource);

                FileInfo screenshotsPath = new FileInfo($"{Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)}\\screenshots\\{filename}.png");
                Directory.CreateDirectory(screenshotsPath.DirectoryName);
                ITakesScreenshot screenshotTaker = (ITakesScreenshot)DriverManager.Current;
                Screenshot screenshot = screenshotTaker.GetScreenshot();
                screenshot.SaveAsFile(screenshotsPath.FullName, ScreenshotImageFormat.Png);
            }
        }
        [OneTimeTearDown]
        public void OneTimeTearDown()
        {
            DriverManager.Current.Quit();
        }

        [Test]
        public void VerifyCreateAccountWithEmptyEmail()
        {
            AcmePages.HomePage.ClickSignIn();
            AcmePages.AuthenticationPage.ClickCreateAccount();
            var actualError = AcmePages.AuthenticationPage.GetError();
            Assert.AreEqual(AcmeStringResources.InvalidEmailAddressError, actualError);

        }
        [Test]
        public void VerifyCreateAccountWithInvalidEmail()
        {
            AcmePages.HomePage.ClickSignIn();
            AcmePages.AuthenticationPage.SetEmail("blablabla");
            AcmePages.AuthenticationPage.ClickCreateAccount();
            var actualError = AcmePages.AuthenticationPage.GetError();
            Assert.AreEqual(AcmeStringResources.InvalidEmailAddressError, actualError);
        }
        [Test]
        public void VerifyCreateAccountWithValidEmail()
        {
            var validEmail = "mailname@gmail.com";
            AcmePages.HomePage.ClickSignIn();
            AcmePages.AuthenticationPage.SetEmail(validEmail);
            AcmePages.AuthenticationPage.ClickCreateAccount();

            var actualEmail = AcmePages.CreateAccountePage.GetEmail();

            Assert.AreEqual(validEmail, actualEmail);
            //throw new NotImplementedException("Test should be implemented.");
        }
    }
}
