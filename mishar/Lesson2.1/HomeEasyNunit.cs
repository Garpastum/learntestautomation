﻿using NUnit.Framework;
using System.Collections.Generic;

namespace mishar.Lesson2
{
    [TestFixture]
    public class HomeEasyNunit
    {
        private const int FirstNumber = 10;
        private const int SecondNumber = 3;
        private const int ResultMod = 1;

        [Test]
        public void ModNumbersAssert()
        {
            int result = FirstNumber % SecondNumber;
            Assert.AreEqual(ResultMod, result);
        }
        [Test]
        [TestCase(10, 2, 0)]
        [TestCase(10, 3, 1)]
        [TestCase(10, 4, 2)]
        public void ModNumbersAssertParametrized(int firstNumber, int secondNumber, int resultMod)
        {
            int result = firstNumber % secondNumber;
            Assert.AreEqual(resultMod, result);
        }
        [Test]
        public void GreaterOrEqualAssert()
        {
            var result = FirstNumber >= SecondNumber;
            Assert.GreaterOrEqual(result, true);
        }
        [Test]
        [TestCase(5, 4, true)]
        [TestCase(5, 5, true)]
        [TestCase(5, 6, false)]
        public void GreaterOrEqualAssertParametrized(int firstNumber, int secondNumber, bool resultForComparing)
        {
            var result = firstNumber >= secondNumber;
            Assert.GreaterOrEqual(result, resultForComparing);
        }
        [Test]
        public void StringAssertFindText()
        {
            string fullstring = " It is very long string, you can be sure of this.";
            StringAssert.Contains("very long", fullstring);
        }
        public void CollectionAssertExample()
        {
            var subset = new List<string> { "yellow", "blue" };
            var superset = new List<string> { "white", "yellow", "blue", "brown" };
            CollectionAssert.IsSubsetOf(subset, superset);
        }
        [Test]
        public void StringAssertStartText()
        {
            string fullstring = "It is very long string, you can be sure of this.";
            StringAssert.StartsWith("It is", fullstring);
        }
    }
}
