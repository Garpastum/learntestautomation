﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mishar.Lesson2
{
    [TestFixture]
    public class MyFirstNunit
    {
        private const int FirstNumber = 681;
        private const int SecondNumber = 5;
        private const int ResultForAdding = 686;
        private const int ResultForSubtracting = 676;
        private const int ResultForMultiplying = 3405;
        private const double ResultForDividing = 136.2;

        private static Calculator _calculator;
        private Stopwatch _stopwatch;

        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            _calculator = new Calculator();
        }
        [OneTimeTearDown]
        public void OneTimeTearDown()
        {
            _calculator = null;
        }
        [SetUp]
        public void SetUp()
        {
            _stopwatch = Stopwatch.StartNew();
        }
        [TearDown]
        public void TearDown()
        {
            _stopwatch.Stop();
            Console.WriteLine($"Test took: {_stopwatch.ElapsedTicks} ticks");
        }

        [Test]
        public void AddingNumbers()
        {
            int result = _calculator.Add(FirstNumber, SecondNumber);
            Assert.AreEqual(ResultForAdding, result);
        }
        [Test]
        [TestCase(1, 1, 2)]
        [TestCase(0, 1, 1)]
        [TestCase(1, 0, 1)]
        [TestCase(0, 0, 0)]
        public void AddingNumbersParametrized(int firstNumber, int secondNumber, int resultForAdding)
        {
            int result = _calculator.Add(firstNumber, secondNumber);
            Assert.AreEqual(resultForAdding, result);
        }
        [Test]
        [TestCase(1, 1, 0)]
        [TestCase(0, 1, -1)]
        [TestCase(1, 0, 1)]
        [TestCase(0, 0, 0)]
        public void SubtractingNumbersParametrized(int firstNumber, int secondNumber, int resultForSubtracting)
        {
            int result = _calculator.Subtract(firstNumber, secondNumber);
            Assert.AreEqual(resultForSubtracting, result, "Calculator subtracting failed");
        }
        [Test]
        public void SubtractingNumbers()
        {
            int result = _calculator.Subtract(FirstNumber, SecondNumber);
            Assert.AreEqual(ResultForSubtracting, result, "Calculator subtracting failed");
        }
        [Test]
        public void MultiplyingNumbers()
        {
            int result = _calculator.Multiply(FirstNumber, SecondNumber);
            Assert.AreEqual(ResultForMultiplying, result, "Calculator Multiplying failed");
        }
        [Test]
        public void DividingNumbers()
        {
            double result = _calculator.Divide(FirstNumber, SecondNumber);
            Assert.AreEqual(ResultForDividing, result, "Calculator Dividing failed");
        }

    }
}
