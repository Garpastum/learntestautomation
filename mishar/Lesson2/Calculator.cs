﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mishar.Lesson2
{
    class Calculator
    {
        public int Add(int firstNumber, int secondNumber)
        {
            int result = firstNumber + secondNumber;
            //if (firstNumber==0)
            //{
            //    result += 1;
            //}
            return result;
        }
        public int Subtract(int firstNumber, int secondNumber)
        {
            int result = firstNumber - secondNumber;
            //if (firstNumber == 0)
            //{
            //    result -= 1;
            //}
            return result;
        }
        public int Multiply(int firstNumber, int secondNumber)
        {
            int result = firstNumber * secondNumber;
            return result;
        }
        public double Divide(int firstNumber, int secondNumber)
        {
            double result = (double)firstNumber / secondNumber;
            return result;
        }
    }
}
