﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mishar.Lesson2
{
    [TestClass]
    public class MyFirstMsTest
    {
        private const int FirstNumber = 681;
        private const int SecondNumber = 5;
        private const int ResultForAdding = 686;
        private const int ResultForSubtracting = 676;
        private const int ResultForMultiplying = 3405;
        private const double ResultForDividing = 136.2;

        private static Calculator _calculator;
        private Stopwatch _stopwatch;

        [ClassInitialize]
        public static void ClassInitialize(TestContext context)
        {
            _calculator = new Calculator();
        }
        [ClassCleanup]
        public static void ClassCleanup()
        {
            _calculator = null;
        }
        [TestInitialize]
        public void TestInitialize()
        {
            _stopwatch = Stopwatch.StartNew();
        }
        [TestCleanup]
        public void TestCleanup()
        {
            _stopwatch.Stop();
            Console.WriteLine($"Test took: {_stopwatch.ElapsedTicks} ticks");
        }
        [TestMethod]
        public void AddingNumbers()
        {
            int result = _calculator.Add(FirstNumber, SecondNumber);
            Assert.AreEqual(ResultForAdding, result);
        }
        [DataTestMethod]
        [DataRow(1,1,2)]
        [DataRow(0, 1, 1)]
        [DataRow(1, 0, 1)]
        [DataRow(0, 0, 0)]
        public void AddingNumbersParametrized(int firstNumber, int secondNumber, int resultForAdding)
        {
            int result = _calculator.Add(firstNumber, secondNumber);
            Assert.AreEqual(resultForAdding, result);
        }
        [TestMethod]
        public void SubtractingNumbers()
        {
            int result = _calculator.Subtract(FirstNumber, SecondNumber);
            Assert.AreEqual(ResultForSubtracting, result, "Calculator subtracting failed");
        }
        [DataTestMethod]
        [DataRow(1, 1, 0)]
        [DataRow(0, 1, -1)]
        [DataRow(1, 0, 1)]
        [DataRow(0, 0, 0)]
        public void SubtractingNumbersParametrized(int firstNumber, int secondNumber, int resultForSubtracting)
        {
            int result = _calculator.Subtract(firstNumber, secondNumber);
            Assert.AreEqual(resultForSubtracting, result, "Calculator subtracting failed");
        }
        [TestMethod]
        public void MultiplyingNumbers()
        {
            int result = _calculator.Multiply(FirstNumber, SecondNumber);
            Assert.AreEqual(ResultForMultiplying, result, "Calculator Multiplying failed");
        }
        [TestMethod]
        public void DividingNumbers()
        {
            double result = _calculator.Divide(FirstNumber, SecondNumber);
            Assert.AreEqual(ResultForDividing, result, "Calculator Dividing failed");
        }

    }
}
