﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mishar.Lesson2
{
    [TestFixture]
    public class MySecondNunit
    {


        [Test]
        public void AssertExample()
        {
            Assert.IsTrue(6 > 5, "6 is greater than 5");
            // AreEqual, AreNotEqual, IsTrue, IsFalse, Greater
        }
        [Test]
        public void StringAssertExample()
        {
            string welcome = "Misha";
            StringAssert.Contains("Misha", welcome);
        }
        [Test]
        public void CollectionAssertExample()
        {
            var subset = new List<string> { "yellow", "blue" };
            var superset = new List<string> { "white", "yellow", "blue", "brown" };
            CollectionAssert.IsSubsetOf(subset, superset);
        }
    }
}
