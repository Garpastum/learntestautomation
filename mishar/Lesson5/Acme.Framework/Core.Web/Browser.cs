﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace mishar.Lesson5.Acme.Framework.Core.Web
{
    public sealed class Browser
    {
        private IWebDriver _driverWrapper;
        internal Browser(IWebDriver driver)
        {
            _driverWrapper = driver;
        }

        internal IWebElement FindElement(Locator locator)
        {
            var element = _driverWrapper.FindElement(locator.Wrapper);
            return element;
        }

        internal void Quit()
        {
            _driverWrapper.Quit();
        }

        public void Maximize()
        {
            _driverWrapper.Manage().Window.Maximize();
        }

        public void NavigateTo(string url)
        {
            _driverWrapper.Navigate().GoToUrl(url);
        }

        //DriverManager.Current.Manage().Window.Maximize();
        //DriverManager.Current.Navigate().GoToUrl("http://automationpractice.com/index.php");
    }
}
