﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mishar.Lesson5.Acme.Framework.Core.Web.Elements
{
    public class AElement : Element
    {
    }
    public class ButtonElement : Element
    {
    }
    public class DivElement : Element
    {
    }
    public class InputElement : Element
    {
        public string Value => Wrapper.GetAttribute("value");
        public void SetText(string text)
        {
            Wrapper.SendKeys(text);
        }

    }
}
