﻿using mishar.Lesson5.Acme.Framework.Core.Web;
using mishar.Lesson5.Acme.UI;
using mishar.Lesson5.Acme.UI.Common;
using mishar.Lesson5.Acme.UI.Pages;
using NUnit.Framework;
using NUnit.Framework.Interfaces;
using OpenQA.Selenium;
using System;
using System.IO;
using System.Reflection;
using System.Threading;

namespace mishar.Lesson5.Acme.Tests
{
    public class CreateAccountFixture
    {
        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            BrowserManager.InitializeBrowser();
            BrowserManager.Current.Maximize();
            //DriverManager.Current.Manage().Window.Maximize();
        }
        [SetUp]
        public void SetUp()
        {
            BrowserManager.Current.NavigateTo("http://automationpractice.com/index.php");
        }
        
        [OneTimeTearDown]
        public void OneTimeTearDown()
        {
            BrowserManager.Terminate();
        }

        [Test]
        public void VerifyCreateAccountWithEmptyEmail()
        {
            AcmePages.HomePage.ClickSignIn();
            AcmePages.AuthenticationPage.ClickCreateAccount();
            var actualError = AcmePages.AuthenticationPage.GetError();
            Assert.AreEqual(AcmeStringResources.InvalidEmailAddressError, actualError);

        }
        [Test]
        public void VerifyCreateAccountWithInvalidEmail()
        {
            AcmePages.HomePage.ClickSignIn();
            AcmePages.AuthenticationPage.SetEmail("blablabla");
            AcmePages.AuthenticationPage.ClickCreateAccount();
            var actualError = AcmePages.AuthenticationPage.GetError();
            Assert.AreEqual(AcmeStringResources.InvalidEmailAddressError, actualError);
        }
        [Test]
        public void VerifyCreateAccountWithValidEmail()
        {
            var validEmail = "mailname@gmail.com";
            AcmePages.HomePage.ClickSignIn();
            AcmePages.AuthenticationPage.SetEmail(validEmail);
            AcmePages.AuthenticationPage.ClickCreateAccount();

            var actualEmail = AcmePages.CreateAccountPage.GetEmail();

            Assert.AreEqual(validEmail, actualEmail);
            //throw new NotImplementedException("Test should be implemented.");
        }
    }
}
