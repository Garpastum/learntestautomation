﻿namespace mishar.Lesson5
{
    internal class CodingConventions
    {
        private int _age1;
        protected int age2;
        internal int age3; // avoid using internal and public
        public int age4; // avoid using internal and public

        public int Age
        {
            get
            {
                return _age1;
            }
            set
            {
                _age1 = value;
            }
        }

        /*public enum Error
        {
            InvalidAddressError,      // do not repeat enum names in velues
            MissingZipError
        }*/

        public enum Error
        {
            InvalidAddress,
            MissingZip
        }

        public int GetAge => _age1;

        public void SomeMethod()
        {
            // SomeMethod - pascal case (namespace, class name, constructor, method, property, interface with 'I'
            // someMethod - camel case (method arguments, local variable, fild name [if private add '_'])
            // do not use screaming caps var DISPLAYNAME
            // do not use hungarian notiation string strDisplayName=...; var intAge=19;

            //class Element - class SpanElement : Element

            // if method do > 1 thing -> refactor method

            // void CreateUser(string n, int a, bool r) -> void CreateUser(string name, int age, bool registeredDate)

            // Do not add 'Get' or 'Set' prefix for properties
            // Is, Has, Can, Allows, Supports -> you may add such prefixes to properties

            // better naming: HorizontalAligment better than HorizontalAligment, CanScrollHorizontally is better than ScrollableX
            // elementCollection.GetDigit/Number/Int() -> elementCollection.GetCount(); -> elementCollection.Count;
            int @int = 15; //avoid

            // var dropDownValue1=... var dropDownValue2=... dropDownValueN=... - avoid
            // var dropDownDefaultValue=... //use

            // CallUser(user, callService)
        }
    }
}