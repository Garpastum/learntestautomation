﻿using mishar.Lesson5.Acme.Framework.Core.Web;
using mishar.Lesson5.Acme.Framework.Core.Web.Elements;

namespace mishar.Lesson5.Acme.UI.Pages
{
    public class CreateAccountPage
    {
        #region PageMapping

        private InputElement Email => ElementFactory.Create<InputElement>(Locator.Id("email"));

        #endregion PageMapping

        #region PageObjects

        public string GetEmail()
        {
            var email = Email.Value;
            return email;
        }

        #endregion PageObjects
    }
}