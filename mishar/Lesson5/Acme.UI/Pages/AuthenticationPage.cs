﻿using mishar.Lesson5.Acme.Framework.Core.Web;
using mishar.Lesson5.Acme.Framework.Core.Web.Elements;
using System.Threading;

namespace mishar.Lesson5.Acme.UI.Pages
{
    public class AuthenticationPage
    {
        #region PageMapping

        private InputElement Email => ElementFactory.Create<InputElement>(Locator.Id("email_create"));
        private DivElement CreateAccount => ElementFactory.Create<DivElement>(Locator.Id("SubmitCreate"));
        private DivElement CreateAccountError => ElementFactory.Create<DivElement>(Locator.Id("create_account_error"));

        #endregion PageMapping

        #region PageObjects

        public void ClickCreateAccount()
        {
            CreateAccount.Click();
            Thread.Sleep(2000);
        }

        public string GetError()
        {
            var error = CreateAccountError.Text;
            return error;
        }

        public void SetEmail(string value)
        {
            Email.SetText(value);
        }

        #endregion PageObjects
    }
}