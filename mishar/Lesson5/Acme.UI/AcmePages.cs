﻿using mishar.Lesson5.Acme.UI.Pages;

namespace mishar.Lesson5.Acme.UI.Pages
{
    public static class AcmePages
    {
        public static CreateAccountPage CreateAccountPage => new CreateAccountPage();
        public static AuthenticationPage AuthenticationPage => new AuthenticationPage();
        public static HomePage HomePage => new HomePage();
    }
}