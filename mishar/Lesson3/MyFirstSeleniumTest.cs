﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

namespace mishar.Lesson3
{
    [TestFixture]
    public class MyFirstSeleniumTest
    {
        private IWebDriver _driver;
        
        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            _driver = new ChromeDriver();
            _driver.Manage().Window.Maximize();
        }
        [SetUp]
        public void SetUp()
        {
            _driver.Navigate().GoToUrl("https://www.google.com/");
        }
        [TearDown]
        public void TearDown()
        {
            _driver.Quit();
        }
        [OneTimeTearDown]
        public void OneTimeTearDown()
        {

        }
        [Test]
        public void GoToApple()
        {
            var expectedUrl = "https://www.apple.com/ru/";
            IWebElement searchField = _driver.FindElement(By.Name("q"));
            searchField.Clear();
            searchField.SendKeys("apple");
            Thread.Sleep(200);

            IWebElement luckyBtn = _driver.FindElement(By.Name("btnI"));
            luckyBtn.Click();

            Assert.AreEqual(expectedUrl, _driver.Url);
            Thread.Sleep(2000);
        }
        [Test]
        public void GoToSoftServe()
        {
            var expectedUrl = "https://www.softserveinc.com/uk-ua";

            IWebElement searchField = _driver.FindElement(By.Name("q"));
            searchField.Clear();
            searchField.SendKeys("softserve");
            Thread.Sleep(200);

            IWebElement luckyBtn = _driver.FindElement(By.Name("btnI"));
            luckyBtn.Click();
            Assert.AreEqual(expectedUrl, _driver.Url);
            Thread.Sleep(2000);
        }
        [Test]
        public void GoToSoftServeExtendedFirst()
        {
            var expectedText = "SoftServe | IT-компанія з консалтингу та розробки ПЗ ...";
            IWebElement searchField = _driver.FindElement(By.Name("q"));

            searchField.SendKeys("softserve");
            Thread.Sleep(200);

            IWebElement searchBtn = _driver.FindElement(By.XPath("//input[@name='btnK']"));
            searchBtn.Click();
            Thread.Sleep(200);
            IWebElement firstTitle = _driver.FindElement(By.CssSelector("h3 > span"));

            Assert.AreEqual(expectedText, firstTitle.Text);
        }
        [Test]
        public void GoToSoftServeExtendedFivth1()
        {
            var expectedText = "SoftServe - Software Company | Facebook - 5,057 Photos";
            IWebElement searchField = _driver.FindElement(By.Name("q"));

            searchField.SendKeys("softserve");
            Thread.Sleep(200);

            IWebElement searchBtn = _driver.FindElement(By.XPath("//input[@name='btnK']"));
            searchBtn.Click();
            IWebElement firstTitle = _driver.FindElement(By.XPath("(//h3/span)[5]"));

            Assert.AreEqual(expectedText, firstTitle.Text);
        }
        [Test]
        public void GoToSoftServeExtendedFivth2()
        {
            var expectedText = "SoftServe - Software Company | Facebook - 5,057 Photos";
            IWebElement searchField = _driver.FindElement(By.Name("q"));

            searchField.SendKeys("softserve");
            Thread.Sleep(200);

            IWebElement searchBtn = _driver.FindElement(By.XPath("//input[@name='btnK']"));
            searchBtn.Click();
            List<IWebElement> firstTitles = _driver.FindElements(By.XPath("//h3/span")).ToList();

            Assert.AreEqual(expectedText, firstTitles[4].Text);
        }
    }
}
