﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;

namespace mishar.Lesson3
{
    [TestFixture]
    public class MySecondSeleniumTest
    {
        private const string FirstName = "Jane";
        private IWebDriver _driver;
        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            _driver = new ChromeDriver();
            _driver.Manage().Window.Maximize();
        }
        [SetUp]
        public void SetUp()
        {
            _driver.Navigate().GoToUrl("https://www.techlistic.com/p/selenium-practice-form.html");
        }
        [TearDown]
        public void TearDown()
        {
            _driver.Quit();
        }
        [OneTimeTearDown]
        public void OneTimeTearDown()
        {

        }
        [Test]
        public void VerifyFirstName()
        {
            IWebElement firstName = _driver.FindElement(By.Name("firstname"));
            firstName.SendKeys(FirstName);

            var firstNameValue = firstName.GetAttribute("value");
            Assert.AreEqual(FirstName, firstNameValue);
        }
        [Test]
        public void VerifyGender()
        {
            IWebElement maleRadio = _driver.FindElement(By.Id("sex-0"));
            IWebElement femaleRadio = _driver.FindElement(By.Id("sex-1"));

            Assert.IsFalse(maleRadio.Selected, "Male radio is checked");
            Assert.IsFalse(femaleRadio.Selected, "Male radio is checked");

            maleRadio.Click();

            Assert.IsTrue(maleRadio.Selected, "Male radio is not checked");
            Assert.IsFalse(femaleRadio.Selected, "Male radio is checked");

            femaleRadio.Click();

            Assert.IsFalse(maleRadio.Selected, "Male radio is checked");
            Assert.IsTrue(femaleRadio.Selected, "Male radio is not checked");

            femaleRadio.Click();

            Assert.IsTrue(maleRadio.Selected, "Male radio is not checked");
            Assert.IsFalse(femaleRadio.Selected, "Male radio is checked");
        }
        [Test]
        public void VerifyContinents()
        {
            IWebElement continentsElement = _driver.FindElement(By.Name("continents"));
            SelectElement continents = new SelectElement(continentsElement);
            Assert.IsFalse(continents.IsMultiple, "User can select multiple continents");
            Assert.AreEqual("Asia", continents.SelectedOption.Text);
            continents.SelectByText("Antartica");

            Assert.AreEqual("Antartica", continents.SelectedOption.Text);
        }
    }
}
