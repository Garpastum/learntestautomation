﻿using mishar.Lesson6.Acme.Framework.Core.Extensions;
using mishar.Lesson6.Acme.Framework.Core.Web;
using mishar.Lesson6.Acme.Framework.Core.Web.Elements;
using mishar.Lesson6.Acme.UI;
using mishar.Lesson6.Acme.UI.Common;
using mishar.Lesson6.Acme.UI.Pages;
using NUnit.Framework;
using NUnit.Framework.Interfaces;
using OpenQA.Selenium;
using System;
using System.IO;
using System.Reflection;
using System.Threading;

namespace mishar.Lesson6.Acme.Tests
{
    public class CreateAccountFixture
    {
        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            BrowserManager.InitializeBrowser();
            BrowserManager.Current.Maximize();
            //DriverManager.Current.Manage().Window.Maximize();
        }
        [SetUp]
        public void SetUp()
        {
            BrowserManager.Current.NavigateTo("http://automationpractice.com/index.php");
        }

        [OneTimeTearDown]
        public void OneTimeTearDown()
        {
            BrowserManager.Terminate();
        }

        [Test]
        public void VerifyCreateAccountWithEmptyEmail()
        {
            AcmePages.HomePage.ClickSignIn();
            AcmePages.AuthenticationPage.ClickCreateAccount();
            var actualError = AcmePages.AuthenticationPage.GetError();
            Assert.AreEqual(AcmeStringResources.InvalidEmailAddressError, actualError);

        }
        [Test]
        public void VerifyCreateAccountWithInvalidEmail()
        {
            AcmePages.HomePage.ClickSignIn();
            AcmePages.AuthenticationPage.SetEmail("blablabla");
            AcmePages.AuthenticationPage.ClickCreateAccount();
            var actualError = AcmePages.AuthenticationPage.GetError();
            Assert.AreEqual(AcmeStringResources.InvalidEmailAddressError, actualError);
        }
        [Test]
        public void VerifyCreateAccountWithValidEmail()
        {
            var validEmail = "mailname@gmail.com";
            AcmePages.HomePage.ClickSignIn();
            AcmePages.AuthenticationPage.SetEmail(validEmail);
            AcmePages.AuthenticationPage.ClickCreateAccount();

            var actualEmail = AcmePages.CreateAccountPage.GetEmail();

            Assert.AreEqual(validEmail, actualEmail);
            //throw new NotImplementedException("Test should be implemented.");
        }

        // tests for showing functionality
        [Test]
        public void VerifyImplicitWait()
        {
            BrowserManager.Current.SetImplicitWait(30);
            AcmePages.AuthenticationPage.SetEmail("mymail@gmail.com");
        }
        [Test]
        public void VerifyExplicitWait()
        {
            AcmePages.AuthenticationPage.SetEmail("mymail@gmail.com");
        }
        [Test]
        public void VerifyDragAndDrop1()
        {
            BrowserManager.Current.NavigateTo("https://www.globalsqa.com/demoSite/practice/droppable/photo-manager.html");
            BrowserManager.Current.WaitForPageSourceIsNotChanged();
            var dragElement = ElementFactory.Create<DivElement>(Locator.Css("#gallery img"));
            var dropElement = ElementFactory.Create<DivElement>(Locator.Id("trash"));
            BrowserManager.Current.DragAndDrop1(dragElement, dropElement);
        }
        [Test]
        public void VerifyDragAndDrop2()
        {
            BrowserManager.Current.NavigateTo("https://www.globalsqa.com/demoSite/practice/droppable/photo-manager.html");
            var dragElement = ElementFactory.Create<DivElement>(Locator.Css("#gallery img"));
            var dropElement = ElementFactory.Create<DivElement>(Locator.Id("trash"));
            BrowserManager.Current.DragAndDrop2(dragElement, dropElement);
        }
        [Test]
        public void VerifyDragAndDropAll()
        {
            BrowserManager.Current.NavigateTo("https://www.globalsqa.com/demoSite/practice/droppable/photo-manager.html");
            //BrowserManager.Current.DragAndDropAll(dragElement, dropElement);
            int count = 0;
            try
            {
                while (true)
                {
                    var dragElement = ElementFactory.Create<DivElement>(Locator.Css("#gallery img"));
                    var dropElement = ElementFactory.Create<DivElement>(Locator.Id("trash"));
                    BrowserManager.Current.WaitForPageSourceIsNotChanged();
                    BrowserManager.Current.DragAndDropAll(dragElement, dropElement);
                    count++;
                }
            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);
            }
            Assert.AreEqual(4, count);
        }
        [Test]
        public void VerifyJavaScript()
        {
            AcmePages.HomePage.ClickSignIn();
            var pathName = (string)BrowserManager.Current.ExecuteJavaScript("return document.location.pathname+document.location.search");
            Assert.AreEqual(AcmePageUrls.AuthenticationPage, pathName);
        }
    }
}
