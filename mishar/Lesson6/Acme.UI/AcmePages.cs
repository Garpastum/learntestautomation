﻿using mishar.Lesson6.Acme.UI.Pages;

namespace mishar.Lesson6.Acme.UI.Pages
{
    public static class AcmePages
    {
        public static CreateAccountPage CreateAccountPage => new CreateAccountPage(AcmePageUrls.CreateAccountPage);
        public static AuthenticationPage AuthenticationPage => new AuthenticationPage(AcmePageUrls.AuthenticationPage);
        public static HomePage HomePage => new HomePage(AcmePageUrls.HomePage);
    }
}