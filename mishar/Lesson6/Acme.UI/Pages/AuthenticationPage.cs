﻿using mishar.Lesson6.Acme.Framework.Core.Web;
using mishar.Lesson6.Acme.Framework.Core.Web.Elements;
using System.Threading;

namespace mishar.Lesson6.Acme.UI.Pages
{
    public class AuthenticationPage : BasePage
    {
        public AuthenticationPage(string pageUrl) : base(pageUrl)
        {
        }
        #region PageMapping

        private InputElement Email => ElementFactory.Create<InputElement>(Locator.Id("email_create"));
        private DivElement CreateAccount => ElementFactory.Create<DivElement>(Locator.Id("SubmitCreate"));
        private DivElement CreateAccountError => ElementFactory.Create<DivElement>(Locator.Id("create_account_error"));

        #endregion PageMapping

        #region PageObjects

        public void ClickCreateAccount()
        {
            CreateAccount.Click();
        }

        public string GetError()
        {
            BrowserManager.Current.WaitForPageSourceIsNotChanged();
            var error = CreateAccountError.Text;
            return error;
        }

        public void SetEmail(string value)
        {
            BrowserManager.Current.WaitForElementIsDisplayed(Email);
            Email.SetText(value);
        }

        #endregion PageObjects
    }
}