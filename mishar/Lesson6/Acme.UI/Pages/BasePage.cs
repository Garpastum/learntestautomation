﻿using mishar.Lesson6.Acme.Framework.Core.Extensions;
using mishar.Lesson6.Acme.Framework.Core.Web;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mishar.Lesson6.Acme.UI.Pages
{
    public abstract class BasePage
    {
        public BasePage(string pageUrl)
        {
            ConditionalWaiter.ForTrue(() => BrowserManager.Current.PageUrl.Contains(pageUrl));
            //BrowserManager.Current.WaitForPageSourceIsNotChanged(); // good for first times
        }
    }
}
