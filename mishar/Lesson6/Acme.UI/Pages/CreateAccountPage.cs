﻿using mishar.Lesson6.Acme.Framework.Core.Web;
using mishar.Lesson6.Acme.Framework.Core.Web.Elements;

namespace mishar.Lesson6.Acme.UI.Pages
{
    public class CreateAccountPage : BasePage
    {
        public CreateAccountPage(string pageUrl) : base(pageUrl)
        {
        }
        #region PageMapping

        private InputElement Email => ElementFactory.Create<InputElement>(Locator.Id("email"));

        #endregion PageMapping

        #region PageObjects

        public string GetEmail()
        {
            var email = Email.Value;
            return email;
        }
        #endregion PageObjects
    }
}