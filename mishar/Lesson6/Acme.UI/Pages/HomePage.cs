﻿using mishar.Lesson6.Acme.Framework.Core.Web;
using mishar.Lesson6.Acme.Framework.Core.Web.Elements;

namespace mishar.Lesson6.Acme.UI.Pages
{
    public class HomePage:BasePage
    {
        public HomePage(string pageUrl) : base(pageUrl)
        {
        }
        #region PageMapping

        //private IWebElement SignInLink => DriverManager.Current.FindElement(By.ClassName("login"));
        private AElement SignInLink => ElementFactory.Create<AElement>(Locator.ClassName("login"));

        #endregion PageMapping

        #region PageObjects

        public void ClickSignIn()
        {
            SignInLink.Click();
        }

        #endregion PageObjects
    }
}