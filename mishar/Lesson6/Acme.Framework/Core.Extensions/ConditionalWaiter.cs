﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mishar.Lesson6.Acme.Framework.Core.Extensions
{
    public static class ConditionalWaiter
    {
        private const int DefaultTimeoutInSeconds = 10;
        public static void ForTrue(Func<bool> anonymousFunction)
        {
            Stopwatch timer = Stopwatch.StartNew();
            do
            {
                if (anonymousFunction())
                {
                    return;
                }
            } while (timer.Elapsed.TotalSeconds < DefaultTimeoutInSeconds);

            throw new TimeoutException("Timeout reached!");
        }
    }
}
