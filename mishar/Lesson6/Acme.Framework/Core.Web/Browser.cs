﻿using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace mishar.Lesson6.Acme.Framework.Core.Web
{
    public sealed class Browser
    {
        private const int PageSourcePollingIntervalInMs = 500;

        private readonly IWebDriver _driverWrapper;

        private readonly IJavaScriptExecutor _javaScriptExecutorWrapper;

        private readonly WebDriverWait _waitWrapper;

        private Actions ActionsWrapper => new Actions(_driverWrapper);

        internal Browser(IWebDriver driver)
        {
            _driverWrapper = driver;
            _javaScriptExecutorWrapper = (IJavaScriptExecutor)_driverWrapper;
            _waitWrapper = new WebDriverWait(_driverWrapper, TimeSpan.FromSeconds(30));
        }

        internal IWebElement FindElement(Locator locator)
        {
            var element = _driverWrapper.FindElement(locator.Wrapper);
            return element;
        }

        internal void Quit()
        {
            _driverWrapper.Quit();
        }

        public string PageSource => _driverWrapper.PageSource;
        public string PageUrl => _driverWrapper.Url;

        public void DragAndDrop1(Element from, Element to)
        {
            ActionsWrapper.DragAndDrop(from.Wrapper, to.Wrapper).Build().Perform();
        }
        public void DragAndDrop2(Element from, Element to)
        {
            ActionsWrapper.ClickAndHold(from.Wrapper).MoveToElement(to.Wrapper).Release().Build().Perform();
        }
        public void DragAndDropAll(Element from, Element to)
        {
            ActionsWrapper.DragAndDrop(from.Wrapper, to.Wrapper).Build().Perform();
            //ActionsWrapper.ClickAndHold(from.Wrapper).MoveToElement(to.Wrapper).Release().Build().Perform();
        }
        public object ExecuteJavaScript(string script)
        {
            var result = _javaScriptExecutorWrapper.ExecuteScript(script);
            return result;
        }
        public void Maximize()
        {
            _driverWrapper.Manage().Window.Maximize();
        }

        public void NavigateTo(string url)
        {
            _driverWrapper.Navigate().GoToUrl(url);
        }
        public void SetImplicitWait(int seconds)
        {
            _driverWrapper.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(seconds);
        }
        public void WaitForElementIsDisplayed(Element element)
        {
            _waitWrapper.Until(driver =>
            {
                try
                {
                    IWebElement el = element.ParentWrapper == null
                    ? driver.FindElement(element.Locator.Wrapper)
                    : element.ParentWrapper.FindElement(element.Locator.Wrapper);
                    return el.Displayed;
                }
                catch
                {
                    return false;
                }
            });
        }
        public void WaitForPageSourceIsNotChanged(int timeoutInSeconds = 10)
        {
            string previousPageSource;
            Stopwatch timer = Stopwatch.StartNew();
            do
            {
                previousPageSource = PageSource;
                Thread.Sleep(PageSourcePollingIntervalInMs);
            } while (PageSource.Equals(previousPageSource) && timer.Elapsed <= TimeSpan.FromSeconds(timeoutInSeconds));

        }
        //DriverManager.Current.Manage().Window.Maximize();
        //DriverManager.Current.Navigate().GoToUrl("http://automationpractice.com/index.php");
    }
}
