﻿namespace mishar.Lesson6.Acme.Framework.Core.Web
{
    public static class ElementFactory
    {
        public static TElement Create<TElement>(Locator locator) where TElement : Element, new()
        {
            var element = new TElement { Locator = locator };
            return element;
        }
    }
}