﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mishar.Lesson6.Acme.Framework.Core.Web
{
    public sealed class Locator
    {
        internal By Wrapper;

        private Locator(By wrapper)
        {
            Wrapper = wrapper;
        }

        public static Locator ClassName(string className)
        {
            var byLocator = By.ClassName(className);
            return new Locator(byLocator);
        }
        public static Locator Css(string cssSelector)
        {
            var byLocator = By.CssSelector(cssSelector);
            return new Locator(byLocator);
        }
        public static Locator Id(string id)
        {
            var byLocator = By.Id(id);
            return new Locator(byLocator);
        }
    }
}
