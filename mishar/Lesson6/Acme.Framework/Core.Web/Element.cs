﻿using mishar.Lesson6.Acme.Framework.Core.Web;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mishar.Lesson6.Acme.Framework
{
    public abstract class Element
    {
        private IWebElement _parentWrapper;
        internal Locator Locator;

        public IWebElement ParentWrapper { get; set; }

        internal IWebElement Wrapper => _parentWrapper == null
            ? BrowserManager.Current.FindElement(Locator)
            : _parentWrapper.FindElement(Locator.Wrapper);

        public string Text => Wrapper.Text;

        public void Click()
        {
            Wrapper.Click();
        }
    }

}
