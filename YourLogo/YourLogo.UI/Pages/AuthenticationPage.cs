﻿using YourLogo.Framework.Core.Web;
using YourLogo.Framework.Core.Web.Elements;
using System.Threading;
using NLog;

namespace YourLogo.UI.Pages
{
    public class AuthenticationPage : BasePage
    {
        private static Logger Logger => LogManager.GetCurrentClassLogger();
        public AuthenticationPage(string pageUrl) : base(pageUrl)
        {
        }
        #region PageMapping

        private InputElement Email => ElementFactory.Create<InputElement>(Locator.Id("email_create"));
        private DivElement CreateAccount => ElementFactory.Create<DivElement>(Locator.Id("SubmitCreate"));
        private DivElement CreateAccountError => ElementFactory.Create<DivElement>(Locator.Id("create_account_error"));

        #endregion PageMapping

        #region PageObjects

        public void ClickCreateAccount()
        {
            Logger.Info("Clicking 'Create account'...");
            CreateAccount.Click();
        }

        public string GetError()
        {
            Logger.Info("Getting error message...");
            BrowserManager.Current.WaitForPageSourceIsNotChanged();
            var error = CreateAccountError.Text;
            Logger.Debug($"[{error}]");
            return error;
        }

        public void SetEmail(string value)
        {
            Logger.Info($"Setting '{value}' to 'Email' field...");
            BrowserManager.Current.WaitForElementIsDisplayed(Email);
            Email.SetText(value);
        }

        #endregion PageObjects
    }
}