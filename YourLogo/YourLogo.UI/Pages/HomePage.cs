﻿using NLog;
using YourLogo.Framework.Core.Web;
using YourLogo.Framework.Core.Web.Elements;

namespace YourLogo.UI.Pages
{
    public class HomePage : BasePage
    {
        private static Logger Logger => LogManager.GetCurrentClassLogger();
        public HomePage(string pageUrl) : base(pageUrl)
        {
        }
        #region PageMapping

        //private IWebElement SignInLink => DriverManager.Current.FindElement(By.ClassName("login"));
        private DivElement GetProductCountItem => ElementFactory.Create<DivElement>(Locator.ClassName("product-count"));
        private DivElement GetSearchError => ElementFactory.Create<DivElement>(Locator.Css("#center_column > p"));
        private AElement SignInLink => ElementFactory.Create<AElement>(Locator.ClassName("login"));
        private InputElement SearchQueryLink => ElementFactory.Create<InputElement>(Locator.Id("search_query_top"));
        private InputElement SearchClickLink => ElementFactory.Create<InputElement>(Locator.Css("#searchbox > button"));
        private AElement SelectProduct => ElementFactory.Create<AElement>(Locator.XPath("//ul[@id='homefeatured']/li[2]/div[@class='product-container']"));
        private AElement AddToCart => ElementFactory.Create<AElement>(Locator.XPath("//p[@id='add_to_cart']//span[.='Add to cart']"));
        private AElement ContinueShopping => ElementFactory.Create<AElement>(Locator.XPath("//*[@id=\"layer_cart\"]/div[1]/div[2]/div[4]/span"));
        private DivElement GetCountInCart => ElementFactory.Create<DivElement>(Locator.XPath("//*[@id=\"header\"]/div[3]/div/div/div[3]/div/a/span[1]"));


        #endregion PageMapping

        #region PageObjects

        public void SelectProductClick()
        {
            BrowserManager.Current.WaitForPageSourceIsNotChanged();
            SelectProduct.Click();
        }
        public void AddToCartClick()
        {
            BrowserManager.Current.WaitForPageSourceIsNotChanged();
            AddToCart.Click();
        }
        public void ContinueShoppingClick()
        {
            BrowserManager.Current.WaitForPageSourceIsNotChanged();
            ContinueShopping.Click();
        }
        public string GetProductCountInCart()
        {
            BrowserManager.Current.WaitForPageSourceIsNotChanged();
            var count = GetCountInCart.Text;
            return count;
        }

        public void ClickSignIn()
        {
            BrowserManager.Current.WaitForPageSourceIsNotChanged();
            SignInLink.Click();
        }
        public string GetProductCount()
        {
            BrowserManager.Current.WaitForPageSourceIsNotChanged();
            var strCount = GetProductCountItem.Text;
            return strCount;
        }
        public void SearchQueryClick()
        {
            BrowserManager.Current.WaitForPageSourceIsNotChanged();
            SearchClickLink.Click();
        }
        public void SetSearchQuery(string value)
        {
            BrowserManager.Current.WaitForElementIsDisplayed(SearchQueryLink);
            SearchQueryLink.SetText(value);
        }
        public string GetNotFoundError()
        {
            BrowserManager.Current.WaitForPageSourceIsNotChanged();
            var strError = GetSearchError.Text;
            return strError;
        }
        #endregion PageObjects
    }
}