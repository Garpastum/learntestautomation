﻿using NLog;
using YourLogo.Framework.Core.Web;
using YourLogo.Framework.Core.Web.Elements;

namespace YourLogo.UI.Pages
{
    public class CreateAccountPage : BasePage
    {
        private static Logger Logger => LogManager.GetCurrentClassLogger();
        public CreateAccountPage(string pageUrl) : base(pageUrl)
        {
        }
        #region PageMapping

        private InputElement Email => ElementFactory.Create<InputElement>(Locator.Id("email"));

        #endregion PageMapping

        #region PageObjects

        public string GetEmail()
        {
            Logger.Info("Getting value from 'Email' from field...");
            var email = Email.Value;
            Logger.Debug($"[{email}]");
            return email;
        }
        #endregion PageObjects
    }
}