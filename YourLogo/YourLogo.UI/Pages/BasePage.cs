﻿using YourLogo.Framework.Core.Extensions;
using YourLogo.Framework.Core.Web;

namespace YourLogo.UI.Pages
{
    public abstract class BasePage
    {
        public BasePage(string pageUrl)
        {
            ConditionalWaiter.ForTrue(() => BrowserManager.Current.PageUrl.Contains(pageUrl));
            //BrowserManager.Current.WaitForPageSourceIsNotChanged(); // good for first times
        }
    }
}