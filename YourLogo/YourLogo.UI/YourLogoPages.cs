﻿using YourLogo.UI.Pages;

namespace YourLogo.UI
{
    public static class YourLogoPages
    {
        public static CreateAccountPage CreateAccountPage => new CreateAccountPage(YourLogoPageUrls.CreateAccountPage);
        public static AuthenticationPage AuthenticationPage => new AuthenticationPage(YourLogoPageUrls.AuthenticationPage);
        public static HomePage HomePage => new HomePage(YourLogoPageUrls.HomePage);
    }
}