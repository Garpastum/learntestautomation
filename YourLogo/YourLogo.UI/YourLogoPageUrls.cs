﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YourLogo.UI
{
    public static class YourLogoPageUrls
    {
        public static string AuthenticationPage => "/index.php?controller=authentication&back=my-account";
        public static string CreateAccountPage => "/index.php?controller=authentication&back=my-account#account-creation";
        public static string HomePage => "/index.php";
    }
}
