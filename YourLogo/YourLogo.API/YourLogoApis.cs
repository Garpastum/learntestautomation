﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YourLogo.Framework.Core.Rest;

namespace YourLogo.API
{
    public class YourLogoApis
    {
        private readonly Client _client;
        public YourLogoApis(string baseUrl)
        {
            _client = new Client(baseUrl);
        }

        public Post GetPost(int id)
        {
            var request = new Request($"posts/{id}", Method.GET);
            var response = _client.Execute<Post>(request);
            return response.Data;
        }

        public class Post
        {
            public string Body { get; set; }
            public int Id { get; set; }
            public string Title { get; set; }
            public int UserId { get; set; }
        }
    }
}
