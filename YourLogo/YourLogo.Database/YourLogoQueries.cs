﻿using YourLogo.Framework.Core.Db;

namespace YourLogo.Database
{
    public class YourLogoQueries
    {
        private readonly MsSqlCommandManager _commandManager;

        public YourLogoQueries(string connectionString)
        {
            _commandManager = new MsSqlCommandManager(connectionString);
        }

        public void CreateUser(string name, int age)
        {
            _commandManager.RunCommand($"INSERT INTO Users(name, age) VALUES(`{name}`,`{age}`)");
        }
    }
}