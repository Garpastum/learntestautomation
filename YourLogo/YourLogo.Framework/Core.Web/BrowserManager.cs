﻿using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YourLogo.Framework.Core.Web
{
    public sealed class BrowserManager
    {
        [ThreadStatic]
        private static Browser _browser;

        public static Browser Current => _browser;
        public static void InitializeBrowser()
        {
            _browser = new Browser(new ChromeDriver());
        }
        public static void Terminate()
        {
            _browser.Quit();
            _browser = null;
        }
    }
}
