﻿namespace YourLogo.Framework.Core.Web.Elements
{
    public class AElement : Element
    {
    }

    public class ButtonElement : Element
    {
    }

    public class DivElement : Element
    {
    }

    public class InputElement : Element
    {
        public string Value => Wrapper.GetAttribute("value");

        public void SetText(string text)
        {
            Wrapper.SendKeys(text);
        }
    }
}