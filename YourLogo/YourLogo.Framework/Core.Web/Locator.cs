﻿using OpenQA.Selenium;

namespace YourLogo.Framework.Core.Web
{
    public sealed class Locator
    {
        internal By Wrapper;

        private Locator(By wrapper)
        {
            Wrapper = wrapper;
        }

        public static Locator ClassName(string className)
        {
            var byLocator = By.ClassName(className);
            return new Locator(byLocator);
        }

        public static Locator Css(string cssSelector)
        {
            var byLocator = By.CssSelector(cssSelector);
            return new Locator(byLocator);
        }

        public static Locator Id(string id)
        {
            var byLocator = By.Id(id);
            return new Locator(byLocator);
        }
        public static Locator XPath(string xpath)
        {
            var byLocator = By.XPath(xpath);
            return new Locator(byLocator);
        }

        public override string ToString()
        {
            return Wrapper.ToString();
        }
    }
}