﻿using OpenQA.Selenium;

namespace YourLogo.Framework.Core.Web
{
    public abstract class Element
    {
        private IWebElement _parentWrapper;
        internal Locator Locator;

        public IWebElement ParentWrapper { get; set; }

        internal IWebElement Wrapper => _parentWrapper == null
            ? BrowserManager.Current.FindElement(Locator)
            : _parentWrapper.FindElement(Locator.Wrapper);

        public string Text => Wrapper.Text;

        public void Click()
        {
            Wrapper.Click();
        }
    }
}