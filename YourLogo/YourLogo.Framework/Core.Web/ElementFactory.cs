﻿using NLog;

namespace YourLogo.Framework.Core.Web
{
    public static class ElementFactory
    {
        private static Logger Logger => LogManager.GetCurrentClassLogger();
        public static TElement Create<TElement>(Locator locator) where TElement : Element, new()
        {
            Logger.Trace($"Creating element with {locator} locator...");
            var element = new TElement { Locator = locator };
            return element;
        }
    }
}