﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YourLogo.Framework.Core.Rest
{
    public class Client
    {
        internal RestClient Wrapper;
        public Client(string url)
        {
            Wrapper = new RestClient(url);
        }
        public IRestResponse<T> Execute<T>(Request request)
        {
            var response = Wrapper.Execute<T>(request.Wrapper);
            return response;
        }
    }
}
