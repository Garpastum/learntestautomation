﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YourLogo.Framework.Core.Rest
{
    public class Request
    {
        internal RestRequest Wrapper;
        public Request(string url, Method method)
        {
            Wrapper = new RestRequest(url, method);
        }
        public void AddParameter(string name, string value)
        {
            Wrapper.AddParameter(name, value);
        }
    }
}
