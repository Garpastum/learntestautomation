﻿using System;
using System.Diagnostics;

namespace YourLogo.Framework.Core.Extensions
{
    public static class ConditionalWaiter
    {
        private const int DefaultTimeoutInSeconds = 10;

        public static void ForTrue(Func<bool> anonymousFunction)
        {
            Stopwatch timer = Stopwatch.StartNew();
            do
            {
                if (anonymousFunction())
                {
                    return;
                }
            } while (timer.Elapsed.TotalSeconds < DefaultTimeoutInSeconds);

            throw new TimeoutException("Timeout reached!");
        }
    }
}