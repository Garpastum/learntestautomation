﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YourLogo.Framework.Core.Db
{
    public class MsSqlCommandManager
    {
        private const int CommandTimeoutInSeconds = 60;
        private readonly string _connectionString;
        public MsSqlCommandManager(string connectionString)
        {
            _connectionString = connectionString;
        }
        public string RunCommand(string query)
        {
            using(SqlConnection connection = new SqlConnection(_connectionString))
            {
                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    connection.Open();
                    command.CommandTimeout = CommandTimeoutInSeconds;
                    var result = command.ExecuteScalar()?.ToString();
                    return result;
                }
            }
        }
    }
}
