﻿using System.Collections.Generic;
using YourLogo.API;

namespace YourLogo.Tests
{
    public class PostRepository
    {
        public string ApiUrl { get; set; }
        public YourLogoApis.Post Post { get; set; }
    }
}