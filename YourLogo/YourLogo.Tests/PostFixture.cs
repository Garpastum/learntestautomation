﻿using Allure.Commons;
using Newtonsoft.Json;
using NUnit.Allure.Attributes;
using NUnit.Allure.Core;
using NUnit.Framework;
using NUnit.Framework.Interfaces;
using System;
using System.IO;
using System.Reflection;
using YourLogo.API;
using YourLogo.Database;
using YourLogo.Framework.Core.Web;
using YourLogo.UI;
using YourLogo.UI.Common;

namespace YourLogo.Tests
{
    [AllureNUnit]
    [TestFixture]
    public class PostFixture
    {
        private const string TestDataFileName = "PostData.json";
        private PostRepository Repository { get; set; }
        private YourLogoApis YourLogoApis { get; set; }

        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            //Environment.CurrentDirectory = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            Repository = GetRepository();

            YourLogoApis = new YourLogoApis(Repository.ApiUrl);
        }

        private PostRepository GetRepository()
        {
            var pathToDataFile = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, TestDataFileName);
            var jsonText = File.ReadAllText(pathToDataFile);
            return JsonConvert.DeserializeObject<PostRepository>(jsonText);
        }

        [Test]
        public void VerifyGettingPost()
        {
            var post = YourLogoApis.GetPost(Repository.Post.Id);
            Assert.Multiple(() =>
            {
                Assert.AreEqual(Repository.Post.Body, post.Body);
                Assert.AreEqual(Repository.Post.Id, post.Id);
                Assert.AreEqual(Repository.Post.Title, post.Title);
                Assert.AreEqual(Repository.Post.UserId, post.UserId);
            });
        }
    }
}