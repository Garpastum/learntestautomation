﻿using Newtonsoft.Json;
using NUnit.Framework;
using NUnit.Framework.Interfaces;
using System;
using System.IO;
using YourLogo.Framework.Core.Web;
using YourLogo.UI;

namespace YourLogo.Tests
{
    public class SearchFixture
    {
        private const string TestDataFileName = "SearchData.json";
        private SearchRepository SearchRepository { get; set; }

        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            SearchRepository = GetRepository();

            BrowserManager.InitializeBrowser();
            BrowserManager.Current.Maximize();
        }

        private SearchRepository GetRepository()
        {
            var pathToDataFile = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, TestDataFileName);
            var jsonText = File.ReadAllText(pathToDataFile);
            return JsonConvert.DeserializeObject<SearchRepository>(jsonText);
        }

        [SetUp]
        public void SetUp()
        {
            BrowserManager.Current.NavigateTo(SearchRepository.BaseUrl);
        }

        [TearDown]
        public void TearDown()
        {
            if (TestContext.CurrentContext.Result.Outcome.Status.Equals(TestStatus.Failed))
            {
                BrowserManager.Current.SaveScreenShot($"{TestContext.CurrentContext.Test.Name} - {DateTime.Now:yyyy.MM.dd-hh.mm.ss}");
            }
        }

        [OneTimeTearDown]
        public void OneTimeTearDown()
        {
            BrowserManager.Terminate();
        }

        [Test]
        public void SearchDress()
        {
            YourLogoPages.HomePage.SetSearchQuery(SearchRepository.SearchDress);
            YourLogoPages.HomePage.SearchQueryClick();

            var strCount = YourLogoPages.HomePage.GetProductCount();
            var findBeginCountPos = strCount.IndexOf("of ") + 3;
            var findEndCountPos = strCount.IndexOf(" items");

            var expectedCount = "7";
            var actualCount = strCount.Substring(findBeginCountPos, findEndCountPos - findBeginCountPos);

            Assert.AreEqual(expectedCount, actualCount);
        }
        [Test]
        public void SearchNotFound()
        {
            YourLogoPages.HomePage.SetSearchQuery(SearchRepository.SearchError);
            YourLogoPages.HomePage.SearchQueryClick();

            var expectedError = $"No results were found for your search \"{SearchRepository.SearchError}\"";
            var actualError = YourLogoPages.HomePage.GetNotFoundError();

            Assert.AreEqual(expectedError, actualError);
        }
    }
}