﻿using System.Collections.Generic;

namespace YourLogo.Tests
{
    public class CreateAccountRepository
    {
        public string BaseUrl { get; set; }
        public string ConnectionString { get; set; }
        public string InvalidEmail { get; set; }
        public string ValidEmail { get; set; }
        public string SelectProduct { get; set; }

        // for example
        public Person Admin { get; set; }
    }
    // for example
    public class Person
    {
        public string Name { get; set; }
        public int Age { get; set; }
        public List<string> Roles { get; set; }
    }
}