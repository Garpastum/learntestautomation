﻿using Allure.Commons;
using Newtonsoft.Json;
using NUnit.Allure.Attributes;
using NUnit.Allure.Core;
using NUnit.Framework;
using NUnit.Framework.Interfaces;
using System;
using System.IO;
using System.Reflection;
using YourLogo.Database;
using YourLogo.Framework.Core.Web;
using YourLogo.UI;
using YourLogo.UI.Common;

namespace YourLogo.Tests
{
    [AllureNUnit]
    [TestFixture]
    public class CreateAccountFixture
    {
        private const string TestDataFileName = "CreateAccountData.json";
        private CreateAccountRepository Repository { get; set; }
        private YourLogoQueries YourLogoQueries { get; set; }

        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            Environment.CurrentDirectory = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            Repository = GetRepository();

            YourLogoQueries = new YourLogoQueries(Repository.ConnectionString);

            //YourLogoQueries.CreateUser(Repository.Admin.Name, Repository.Admin.Age);

            BrowserManager.InitializeBrowser();
            BrowserManager.Current.Maximize();
        }

        private CreateAccountRepository GetRepository()
        {
            var pathToDataFile = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, TestDataFileName);
            var jsonText = File.ReadAllText(pathToDataFile);
            return JsonConvert.DeserializeObject<CreateAccountRepository>(jsonText);
        }

        [SetUp]
        public void SetUp()
        {
            BrowserManager.Current.NavigateTo(Repository.BaseUrl);
        }

        [TearDown]
        public void TearDown()
        {
            if (TestContext.CurrentContext.Result.Outcome.Status.Equals(TestStatus.Failed))
            {
                var screenshotPath = BrowserManager.Current.SaveScreenShot($"{TestContext.CurrentContext.Test.Name} - {DateTime.Now:yyyy.MM.dd-hh.mm.ss}");
                AllureLifecycle.Instance.AddAttachment(screenshotPath, TestContext.CurrentContext.Test.Name);
            }
        }

        [OneTimeTearDown]
        public void OneTimeTearDown()
        {
            BrowserManager.Terminate();
        }

        [Test]
        public void VerifyCreateAccountWithEmptyEmail()
        {
            YourLogoPages.HomePage.ClickSignIn();
            YourLogoPages.AuthenticationPage.ClickCreateAccount();
            var actualError = YourLogoPages.AuthenticationPage.GetError();
            Assert.AreEqual(YourLogoStringResources.InvalidEmailAddressError, actualError);
        }

        [Test]
        public void VerifyCreateAccountWithInvalidEmail()
        {
            YourLogoPages.HomePage.ClickSignIn();
            YourLogoPages.AuthenticationPage.SetEmail(Repository.InvalidEmail);
            YourLogoPages.AuthenticationPage.ClickCreateAccount();
            var actualError = YourLogoPages.AuthenticationPage.GetError();
            Assert.AreEqual(YourLogoStringResources.InvalidEmailAddressError, actualError);
        }

        [Test]
        [Description("This test verifies that email is transisted to 'Create Account' page.")]
        [AllureTag("Registration", "CreateAccount")]
        [AllureSeverity(SeverityLevel.critical)]
        [AllureOwner("jsmith")]
        [AllureParentSuite("YourLogo")]
        [AllureSuite("Regression")]
        [AllureSubSuite("Create Account")]
        [AllureIssue("PRD-1235")]
        [AllureTms("4353465464565")]
        public void VerifyCreateAccountWithValidEmail()
        {
            Environment.CurrentDirectory = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            YourLogoPages.HomePage.ClickSignIn();
            YourLogoPages.AuthenticationPage.SetEmail(Repository.ValidEmail);
            YourLogoPages.AuthenticationPage.ClickCreateAccount();

            var actualEmail = YourLogoPages.CreateAccountPage.GetEmail();
            Assert.AreEqual(Repository.ValidEmail, actualEmail);
        }

        [Test]
        public void AddProductToCart()
        {
            YourLogoPages.HomePage.SelectProductClick();
            YourLogoPages.HomePage.AddToCartClick();
            YourLogoPages.HomePage.ContinueShoppingClick();

            var expectedCount = "1";
            var actualCount = YourLogoPages.HomePage.GetProductCountInCart();

            Assert.AreEqual(expectedCount, actualCount);
        }

        [Test]
        public void TestJson()
        {
            string returnedFromApi = "{\"Name\":\"Bob\",\"Age\":24}";
            Person somePerson = JsonConvert.DeserializeObject<Person>(returnedFromApi);

            Person jane = new Person();
            jane.Name = "Jane";
            jane.Age = 22;

            var json = JsonConvert.SerializeObject(jane); // {"Name":"Jane","Age":22}
        }

        public class Person
        {
            public string Name { get; set; }
            public int Age { get; set; }
        }
    }
}